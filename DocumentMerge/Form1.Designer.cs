﻿namespace DocumentMerge
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.groupBox_chart = new System.Windows.Forms.GroupBox();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.cht2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBox_file = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_period = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button_test = new System.Windows.Forms.Button();
            this.label_test = new System.Windows.Forms.Label();
            this.button_start = new System.Windows.Forms.Button();
            this.textBox_test = new System.Windows.Forms.TextBox();
            this.button_gps = new System.Windows.Forms.Button();
            this.label_gps = new System.Windows.Forms.Label();
            this.textBox_gps = new System.Windows.Forms.TextBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.checkBox_case = new System.Windows.Forms.CheckBox();
            this.groupBox_chart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cht2)).BeginInit();
            this.groupBox_file.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox_chart
            // 
            this.groupBox_chart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox_chart.Controls.Add(this.chart1);
            this.groupBox_chart.Controls.Add(this.cht2);
            this.groupBox_chart.Location = new System.Drawing.Point(21, 17);
            this.groupBox_chart.Name = "groupBox_chart";
            this.groupBox_chart.Size = new System.Drawing.Size(711, 363);
            this.groupBox_chart.TabIndex = 4;
            this.groupBox_chart.TabStop = false;
            this.groupBox_chart.Text = "图表";
            // 
            // chart1
            // 
            chartArea3.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.chart1.Legends.Add(legend3);
            this.chart1.Location = new System.Drawing.Point(50, 45);
            this.chart1.Name = "chart1";
            series3.ChartArea = "ChartArea1";
            series3.Legend = "Legend1";
            series3.Name = "Series1";
            this.chart1.Series.Add(series3);
            this.chart1.Size = new System.Drawing.Size(300, 223);
            this.chart1.TabIndex = 2;
            this.chart1.Text = "chart1";
            // 
            // cht2
            // 
            chartArea4.Name = "ChartArea1";
            this.cht2.ChartAreas.Add(chartArea4);
            legend4.Name = "Legend1";
            this.cht2.Legends.Add(legend4);
            this.cht2.Location = new System.Drawing.Point(379, 45);
            this.cht2.Name = "cht2";
            series4.ChartArea = "ChartArea1";
            series4.Legend = "Legend1";
            series4.Name = "Series1";
            this.cht2.Series.Add(series4);
            this.cht2.Size = new System.Drawing.Size(300, 223);
            this.cht2.TabIndex = 3;
            this.cht2.Text = "chart2";
            // 
            // groupBox_file
            // 
            this.groupBox_file.Controls.Add(this.checkBox_case);
            this.groupBox_file.Controls.Add(this.label1);
            this.groupBox_file.Controls.Add(this.textBox_period);
            this.groupBox_file.Controls.Add(this.button2);
            this.groupBox_file.Controls.Add(this.button1);
            this.groupBox_file.Controls.Add(this.button_test);
            this.groupBox_file.Controls.Add(this.label_test);
            this.groupBox_file.Controls.Add(this.button_start);
            this.groupBox_file.Controls.Add(this.textBox_test);
            this.groupBox_file.Controls.Add(this.button_gps);
            this.groupBox_file.Controls.Add(this.label_gps);
            this.groupBox_file.Controls.Add(this.textBox_gps);
            this.groupBox_file.Location = new System.Drawing.Point(12, 13);
            this.groupBox_file.Name = "groupBox_file";
            this.groupBox_file.Size = new System.Drawing.Size(397, 193);
            this.groupBox_file.TabIndex = 0;
            this.groupBox_file.TabStop = false;
            this.groupBox_file.Text = "文件";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 120);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 8;
            this.label1.Text = "统计周期(S):";
            // 
            // textBox_period
            // 
            this.textBox_period.Location = new System.Drawing.Point(125, 115);
            this.textBox_period.Name = "textBox_period";
            this.textBox_period.Size = new System.Drawing.Size(140, 21);
            this.textBox_period.TabIndex = 7;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(161, 151);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 6;
            this.button2.Text = "显示";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(39, 151);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "隐藏";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button_test
            // 
            this.button_test.Location = new System.Drawing.Point(298, 72);
            this.button_test.Name = "button_test";
            this.button_test.Size = new System.Drawing.Size(75, 23);
            this.button_test.TabIndex = 5;
            this.button_test.Text = "浏览";
            this.button_test.UseVisualStyleBackColor = true;
            this.button_test.Click += new System.EventHandler(this.button_test_Click);
            // 
            // label_test
            // 
            this.label_test.AutoSize = true;
            this.label_test.Location = new System.Drawing.Point(15, 77);
            this.label_test.Name = "label_test";
            this.label_test.Size = new System.Drawing.Size(83, 12);
            this.label_test.TabIndex = 4;
            this.label_test.Text = "路测文件路径:";
            // 
            // button_start
            // 
            this.button_start.Location = new System.Drawing.Point(298, 151);
            this.button_start.Name = "button_start";
            this.button_start.Size = new System.Drawing.Size(75, 23);
            this.button_start.TabIndex = 1;
            this.button_start.Text = "开始";
            this.button_start.UseVisualStyleBackColor = true;
            this.button_start.Click += new System.EventHandler(this.button_start_Click);
            // 
            // textBox_test
            // 
            this.textBox_test.Location = new System.Drawing.Point(125, 73);
            this.textBox_test.Name = "textBox_test";
            this.textBox_test.Size = new System.Drawing.Size(140, 21);
            this.textBox_test.TabIndex = 3;
            // 
            // button_gps
            // 
            this.button_gps.Location = new System.Drawing.Point(298, 24);
            this.button_gps.Name = "button_gps";
            this.button_gps.Size = new System.Drawing.Size(75, 23);
            this.button_gps.TabIndex = 2;
            this.button_gps.Text = "浏览";
            this.button_gps.UseVisualStyleBackColor = true;
            this.button_gps.Click += new System.EventHandler(this.button_gps_Click);
            // 
            // label_gps
            // 
            this.label_gps.AutoSize = true;
            this.label_gps.Location = new System.Drawing.Point(15, 29);
            this.label_gps.Name = "label_gps";
            this.label_gps.Size = new System.Drawing.Size(77, 12);
            this.label_gps.TabIndex = 1;
            this.label_gps.Text = "GPS文件路径:";
            // 
            // textBox_gps
            // 
            this.textBox_gps.Location = new System.Drawing.Point(125, 25);
            this.textBox_gps.Name = "textBox_gps";
            this.textBox_gps.Size = new System.Drawing.Size(140, 21);
            this.textBox_gps.TabIndex = 0;
            // 
            // splitContainer1
            // 
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(12, 12);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.splitContainer1.Panel1.Controls.Add(this.groupBox_file);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.splitContainer1.Panel2.Controls.Add(this.groupBox_chart);
            this.splitContainer1.Size = new System.Drawing.Size(1194, 398);
            this.splitContainer1.SplitterDistance = 423;
            this.splitContainer1.TabIndex = 5;
            // 
            // checkBox_case
            // 
            this.checkBox_case.AutoSize = true;
            this.checkBox_case.Location = new System.Drawing.Point(298, 115);
            this.checkBox_case.Name = "checkBox_case";
            this.checkBox_case.Size = new System.Drawing.Size(54, 16);
            this.checkBox_case.TabIndex = 9;
            this.checkBox_case.Text = "Case5";
            this.checkBox_case.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1218, 421);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "文件合并";
            this.groupBox_chart.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cht2)).EndInit();
            this.groupBox_file.ResumeLayout(false);
            this.groupBox_file.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox_chart;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.DataVisualization.Charting.Chart cht2;
        private System.Windows.Forms.GroupBox groupBox_file;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button_test;
        private System.Windows.Forms.Label label_test;
        private System.Windows.Forms.Button button_start;
        private System.Windows.Forms.TextBox textBox_test;
        private System.Windows.Forms.Button button_gps;
        private System.Windows.Forms.Label label_gps;
        private System.Windows.Forms.TextBox textBox_gps;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TextBox textBox_period;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkBox_case;
    }
}

